# Статьи

[Что каждый программист должен знать о памяти](http://rus-linux.net/lib.php?name=/MyLDP/hard/memory/memory.html) | [What Every Programmer Should Know About Memory](https://people.freebsd.org/~lstewart/articles/cpumemory.pdf)

[Memory Barriers:  a Hardware View for Software Hackers](http://www.rdrop.com/users/paulmck/scalability/paper/whymb.2010.07.23a.pdf)

[Путешествие через вычислительный конвейер процессора](https://habr.com/ru/post/182002/)

# Учебники, лекции

[Теоретические основы организации многопроцессорных комплексов и систем.](https://studfile.net/preview/2968036/)

[Эффективное программирование современных микропроцессоров](http://ssd.sscc.ru/sites/default/files/upload/books/posobie_ngtu_2014_-_effektivnoe_programmirovanie_sovremennyh_mikroprocessorov.pdf)

